.PHONY: all setup clean uninstall build upload-test upload-prod install-test install-prod verify-install-test verify-install-prod

# Package name for reference
PACKAGE_NAME = qanat


# Default target is all, runs the test pipeline
all: setup clean uninstall build upload-test install-test verify-install-test

# Install twine
setup:
	@echo "Installing twine..."
	pip install twine

# Clean previous builds
clean:
	@echo "Cleaning up dist, build, and egg-info directories..."
	rm -rf dist build $(PACKAGE_NAME).egg-info

# Uninstall the package, if installed
uninstall:
	@echo "Uninstalling the package..."
	pip uninstall -y $(PACKAGE_NAME)

# Build the package
build:
	@echo "Building the package..."
	python3 setup.py sdist bdist_wheel

# Upload to Test PyPI
upload-test: setup clean build
	@echo "Uploading the package to Test PyPI..."
	twine upload --repository testpypi dist/*

# Install the package from Test PyPI
install-test: uninstall
	@echo "Installing the package from Test PyPI..."
	pip install --index-url --repository testpypi --extra-index-url https://pypi.org/simple $(PACKAGE_NAME)

# Verify the installation from Test PyPI
verify-install-test:
	@echo "Verifying the installation from Test PyPI..."
	python -c "import $(PACKAGE_NAME); print($(PACKAGE_NAME).__version__)"

# Full Test Pipeline
test-pipeline: upload-test install-test verify-install-test

# Upload to PyPI
upload-prod: setup clean build
	@echo "Uploading the package to PyPI..."
	twine upload dist/*

# Install the package from PyPI
install-prod: uninstall
	@echo "Installing the package from PyPI..."
	pip install $(PACKAGE_NAME)

# Verify the installation from PyPI
verify-install-prod:
	@echo "Verifying the installation from PyPI..."
	python -c "import $(PACKAGE_NAME); print($(PACKAGE_NAME).__version__)"

# Full Production Pipeline
prod-pipeline: upload-prod install-prod verify-install-prod

# Usage: make <target>
# Example: make test-pipeline
# Example: make prod-pipeline
